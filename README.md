Welcome to FLASHboard!!

FLASHboard is a custom dashboard aimed to replace the current SmartDashboard provided by FIRST.
It's main goal is to improve upon the SmartDashboard and provide drivers with more information.

The FLASHboard works directly with FLASHLib which automatically provides relevent data from different 
sources, such as:
 - Human Interface Devices
 - Sensors
 - Cameras
 - Image processing using openCV
 - The roboRIO log tracking
 - Motor output tracking
 - PDP voltage and current tracking
 - And custom data sources
 
Data can also be sent from the dashboard to the robot in the form of buttons, sliders and possibly more.

Please note that this software is early in development and is far from ready for field usage.


This is a product of FRC team 3388 FLASH.